<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('template', 'form_validation'));
      $this->load->model('admin');
	}

   public function index()
   {
		$this->cek_login();
		$data['data'] = $this->admin->get_all('t_items');

		$this->template->admin('admin/manage_item', $data);
   }

   public function add_item()
   {
		$this->cek_login();

      if ($this->input->post('submit', TRUE) == 'Submit') {
         //validasi
         $this->form_validation->set_rules('nama', 'Nama Item', 'required|min_length[4]');
         $this->form_validation->set_rules('harga', 'Harga Item', 'required|numeric');
         $this->form_validation->set_rules('berat', 'Berat Item', 'required|numeric');
         $this->form_validation->set_rules('status', 'Status', 'required|numeric');
         $this->form_validation->set_rules('desk', 'Deskripsi', 'required|min_length[4]');

         if ($this->form_validation->run() == TRUE)
         {
				$config['upload_path'] = './assets/upload/';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = '2048';
				$config['file_name'] = 'gambar'.time();

				$this->load->library('upload', $config);

				if ($this->upload->do_upload('foto'))
				{
					$gbr = $this->upload->data();
					//proses insert
		         $items = array (
		            'nama_item' => $this->input->post('nama', TRUE),
		            'harga' => $this->input->post('harga', TRUE),
		            'berat' => $this->input->post('berat', TRUE),
		            'status' => $this->input->post('status', TRUE),
						'gambar' => $gbr['file_name'],
		            'deskripsi' => $this->input->post('desk', TRUE)
		         );

		        	$this->admin->insert('t_items', $items);

					redirect('item');

				} else {
					$this->session->set_flashdata('alert', 'anda belum memilih foto');
				}
         }
      }

      $data['nama'] = $this->input->post('nama', TRUE);
      $data['berat'] = $this->input->post('berat', TRUE);
      $data['harga'] = $this->input->post('harga', TRUE);
      $data['status'] = $this->input->post('status', TRUE);
      $data['desk'] = $this->input->post('desk', TRUE);

      $data['header'] = "Add New Item";

      $this->template->admin('admin/item_form', $data);
   }

	public function detail()
	{
		$this->cek_login();
		$id_item = $this->uri->segment(3);
		$item = $this->admin->get_where('t_items', array('id_item' => $id_item));

		foreach ($item->result() as $key) {
			$data['nama_item'] = $key->nama_item;
			$data['harga'] = $key->harga;
			$data['berat'] = $key->berat;
			$data['status'] = $key->status;
			$data['gambar'] = $key->gambar;
			$data['deskripsi'] = $key->deskripsi;
		}

		$this->template->admin('admin/detail_item', $data);
	}

	public function update_item()
   {
		$this->cek_login();
		$id_item = $this->uri->segment(3);

      if ($this->input->post('submit', TRUE) == 'Submit') {
         //validasi
         $this->form_validation->set_rules('nama', 'Nama Item', 'required|min_length[4]');
         $this->form_validation->set_rules('harga', 'Harga Item', 'required|numeric');
         $this->form_validation->set_rules('berat', 'Berat Item', 'required|numeric');
         $this->form_validation->set_rules('status', 'Status', 'required|numeric');
         $this->form_validation->set_rules('desk', 'Deskripsi', 'required|min_length[4]');

         if ($this->form_validation->run() == TRUE)
         {
				$config['upload_path'] = './assets/upload/';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = '2048';
				$config['file_name'] = 'gambar'.time();

				$this->load->library('upload', $config);

				$items = array (
					'nama_item' => $this->input->post('nama', TRUE),
					'harga' => $this->input->post('harga', TRUE),
					'berat' => $this->input->post('berat', TRUE),
					'status' => $this->input->post('status', TRUE),
					'deskripsi' => $this->input->post('desk', TRUE)
				);

				if ($this->upload->do_upload('foto'))
				{
					$gbr = $this->upload->data();
					//proses insert
					unlink('assets/upload/'.$this->input->post('old_pict', TRUE));
					$items['gambar'] = $gbr['file_name'];

		         $this->admin->update('t_items', $items, array('id_item' => $id_item));
				} else {
					$this->admin->update('t_items', $items, array('id_item' => $id_item));
				}

				redirect('item');
         }
      }

		$item = $this->admin->get_where('t_items', array('id_item' => $id_item));

		foreach($item->result() as $key) {
	      $data['nama'] = $key->nama_item;
	      $data['berat'] = $key->berat;
	      $data['harga'] = $key->harga;
	      $data['status'] = $key->status;
	      $data['desk'] = $key->deskripsi;
			$data['gambar'] = $key->gambar;
		}

      $data['header'] = "Update Data Item";

      $this->template->admin('admin/item_form', $data);
   }

	function cek_login()
	{
		if (!$this->session->userdata('admin'))
		{
			redirect('login');
		}
	}
}
